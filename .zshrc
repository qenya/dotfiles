# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd extendedglob nomatch
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/timon/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Allow comments in interactive shell
setopt interactivecomments

# Aliases
alias grep='grep --color=auto'
alias visudo='EDITOR=nano visudo'
alias reboot='sudo systemctl reboot'
alias poweroff='sudo systemctl poweroff'
alias l='ls --color=auto'
alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias exeunt='exit'
alias dafuq='man'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Custom executables directory
export PATH=~/bin:$PATH

# Powerline
powerline-daemon -q
. /usr/lib/python3.8/site-packages/powerline/bindings/zsh/powerline.zsh
